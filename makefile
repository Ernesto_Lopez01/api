CFLAGS = -c

bin/ejecutable: out_make/main.o
	$(CC) out_make/main.o -o bin/ejecutable


out_make/main.o:
	$(CC) $(CFLAGS) -Iinclude src/main.c -o out_make/main.o

clean:
	$(RM) *.o bin/ejecutable core
	$(RM) *.o out_make/main.o core

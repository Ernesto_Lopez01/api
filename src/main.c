#include <stdio.h>
#include "lista.c"


int main(){
    printf("Listas Enlazadas de tipo INT\n");
    nodo *lista;
    lista = crearListaINT(lista);

    lista = insertarNodoFinalINT(4,lista);
    lista = insertarNodoFinalINT(10,lista);
    lista = insertarNodoInicioINT(8,lista);
    lista = insertarNodoInicioINT(7,lista);
    imprimirListaINT(lista);

    lista = retirarInicioINT(lista);
    imprimirListaINT(lista);

    lista = retirarFinalINT(lista);
    imprimirListaINT(lista);

    printf("\n\n");
    printf("Listas Enlazadas de tipo FLOAT\n");
    nodoF *listaF;
    listaF = crearListaFLOAT(listaF);

    listaF = insertarNodoFinalFLOAT(4.8,listaF);
    listaF = insertarNodoFinalFLOAT(10.1,listaF);
    listaF = insertarNodoInicioFLOAT(8.2,listaF);
    listaF = insertarNodoInicioFLOAT(7.6,listaF);
    imprimirListaFLOAT(listaF);

    listaF = retirarInicioFLOAT(listaF);
    imprimirListaFLOAT(listaF);

    listaF = retirarFinalFLOAT(listaF);
    imprimirListaFLOAT(listaF);

    return 0;
}



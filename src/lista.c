#include "lista.h"
#include <stdio.h>
#include <stdlib.h>

nodo *crearListaINT(nodo *lista){   
    return lista = NULL;
}

nodo *insertarNodoInicioINT(int valor, nodo *lista){    
    nodo *nodoNuevo;   
    nodoNuevo = (nodo *)malloc(sizeof(nodo));   
    if(nodoNuevo != NULL){
        nodoNuevo -> dato = valor;
        nodoNuevo -> siguiente = lista;
        lista = nodoNuevo;
    }   
    return lista;
}


nodo *insertarNodoFinalINT(int valor, nodo *lista){   
    nodo *nodoNuevo, *nodoAuxiliar;
    nodoNuevo = (nodo *) malloc(sizeof(nodo));   
    if(nodoNuevo != NULL){      
        nodoNuevo -> dato = valor;
        nodoNuevo -> siguiente = NULL;     
        if(lista==NULL){         
            lista=nodoNuevo;
        }else{         
            nodoAuxiliar = lista;          
            while (nodoAuxiliar -> siguiente != NULL){            
                nodoAuxiliar = nodoAuxiliar -> siguiente;
            }  
            nodoAuxiliar -> siguiente = nodoNuevo;
            }
    } 
    return lista;
}


void imprimirListaINT(nodo *lista){ 
    nodo *nodoAuxiliar;
    nodoAuxiliar = lista;
    printf("Inicio Lista Int: ");   
    while (nodoAuxiliar != NULL){
        printf("%d ", nodoAuxiliar -> dato);    
        nodoAuxiliar = nodoAuxiliar -> siguiente;
    }
    printf("NULL\n");
}


nodo *retirarInicioINT(nodo *lista){    
    nodo *nodoAuxiliar;    
    int dato;  
    if(lista == NULL){
        printf("--ERROR--\nNo existen elementos en la lista INT");
    }else{   
        nodoAuxiliar = lista;
        dato = nodoAuxiliar -> dato;
        lista = nodoAuxiliar -> siguiente;
        printf("Se Elimino %d del inicio\n", dato);  
        free(nodoAuxiliar);
    }
 
    return lista;
}


nodo *retirarFinalINT(nodo *lista){  
    nodo *nodoAuxiliar, *nodoAnterior;  
    int dato;  
    if(lista == NULL){
        printf("--ERROR--\nNo existen elementos en la lista INT");
    }else{  
        nodoAuxiliar = lista; 
        while(nodoAuxiliar -> siguiente != NULL){
            nodoAnterior = nodoAuxiliar;
            nodoAuxiliar = nodoAuxiliar -> siguiente;
        }
        dato = nodoAuxiliar -> dato;
        nodoAnterior -> siguiente = NULL;
        printf("Se Elimino %d del final\n", dato);
  
        free(nodoAuxiliar);
    }
  
    return lista;
}

// ----> Listas enlazadas de tipo float(decimal)
// Creación de una lista
nodoF *crearListaFLOAT(nodoF *listaF){
    return listaF = NULL;
}


nodoF *insertarNodoInicioFLOAT(float valorF, nodoF *listaF){  
    nodoF *nodoNuevoF;    
    nodoNuevoF = (nodoF *)malloc(sizeof(nodoF));  
    if(nodoNuevoF != NULL){
        nodoNuevoF -> datoF = valorF;
        nodoNuevoF -> siguienteF = listaF;
        listaF = nodoNuevoF;
    }
  
    return listaF;
}


nodoF *insertarNodoFinalFLOAT(float valorF, nodoF *listaF){ 
    nodoF *nodoNuevoF, *nodoAuxiliarF;  
    nodoNuevoF = (nodoF *) malloc(sizeof(nodoF));  
    if(nodoNuevoF != NULL){      
        nodoNuevoF -> datoF = valorF;
        nodoNuevoF -> siguienteF = NULL;     
        if(listaF==NULL){       
            listaF=nodoNuevoF;
        }else{        
            nodoAuxiliarF = listaF;        
            while (nodoAuxiliarF -> siguienteF != NULL){           
                nodoAuxiliarF = nodoAuxiliarF -> siguienteF;
            }          
            nodoAuxiliarF -> siguienteF = nodoNuevoF;
        }   
    }
  
    return listaF;
}


void imprimirListaFLOAT(nodoF *listaF){
    nodoAuxiliarF = listaF;
    printf("Inicio Lista Float: ");
    while (nodoAuxiliarF != NULL){  
        printf("%.3f", nodoAuxiliarF -> datoF);
        nodoAuxiliarF = nodoAuxiliarF -> siguienteF;
    }
    printf("NULL\n");
}


nodoF *retirarInicioFLOAT(nodoF *listaF){
    nodoF *nodoAuxiliarF;  
    float datoF;
    if(listaF == NULL){
        printf("--ERROR--\nNo existen elementos en la lista FLOAT");
    }else{   
        nodoAuxiliarF = listaF;
        datoF = nodoAuxiliarF -> datoF;
        listaF = nodoAuxiliarF -> siguienteF;
        printf("Se Elimino %.3f del inicio\n", datoF);   
        free(nodoAuxiliarF);
    }

    return listaF;
}


nodoF *retirarFinalFLOAT(nodoF *listaF){
    nodoF *nodoAuxiliarF, *nodoAnteriorF;   
    float datoF;  
    if(listaF == NULL){
        printf("--ERROR--\nNo existen elementos en la lista FLOAT");
    }else{
        nodoAuxiliarF = listaF;
        while(nodoAuxiliarF -> siguienteF != NULL){
            nodoAnteriorF = nodoAuxiliarF;
            nodoAuxiliarF = nodoAuxiliarF -> siguienteF;
        }
        datoF = nodoAuxiliarF -> datoF;
        nodoAnteriorF -> siguienteF = NULL;
        printf("Se Elimino %.3f del final\n", datoF);
    
        free(nodoAuxiliarF);
    }
 
    return listaF;
}

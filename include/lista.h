#ifndef LISTA_H
#define LISTA_H

struct structNodo{
    int dato;    
    struct structNodo *siguiente;
};
typedef struct structNodo nodo;

nodo *crearListaINT(nodo *lista);

nodo *insertarNodoInicioINT(int valor, nodo *lista);

nodo *insertarNodoFinalINT(int valor, nodo *lista);

void imprimirListaINT(nodo *lista);

nodo *retirarInicioINT(nodo *lista);

nodo *retirarFinalINT(nodo *lista);


//----------Listas Enlazadas de tipo Float----------
//Definición de la clase Nodo
struct structNodoF{
    
    float datoF;
    
    struct structNodoF *siguienteF;
};

typedef struct structNodoF nodoF;


nodoF *insertarNodoInicioFLOAT(float valorF, nodoF *listaF);

nodoF *insertarNodoFinalFLOAT(float valorF, nodoF *listaF);

void imprimirListaFLOAT(nodoF *listaF);

nodoF *retirarInicioFLOAT(nodoF *listaF);

nodoF *retirarFinalFLOAT(nodoF *listaF);

#endif //LISTA_H
